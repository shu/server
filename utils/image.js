const multer = require('multer')

const modifyImage = async (req, opts) => {
  if (req.file) {
    opts.imageUrl = `${process.env.HOST}/image/${req.body.category}/${req.file.filename}`
    return opts
  }
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `./public/image/${req.body.category}/`)
  },

  filename: (req, file, cb) => {
    // ex mimetype:"image/png" need to get png
    const type = file.mimetype.split('/')[1]
    // ex product-3.png
    const fileName = `${req.body.category}-${req.body.name}.${type}`
    cb(null, fileName)
  },
})
// Multer is a node.js middleware for handling multipart/form-data
const uploadImage = multer({ storage }).single('image')

module.exports = {
  modifyImage,
  uploadImage,
}
