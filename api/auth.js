const router = require('express').Router()
const axios = require('axios')

const knex = require(`${rootPath}/server/db`)
const jsonwebtoken = require('jsonwebtoken')
const apiProcess = require('./apiProcess')

const { auth } = require(`${rootPath}/server/auth`)
router.post('/loginWithFB', async (req, res, next) => {
  loginWithFB._controller(req, res)
})
let loginWithFB = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.code = req.body.code
    opts.locale = req.body.locale

    return opts
  },

  async run(opts) {
    const data = {
      fb: {},
      locale: {},
    }
    const fbData = await axios.get(
      'https://graph.facebook.com/v4.0/oauth/access_token',
      {
        params: {
          client_id: process.env.FB_CLIENT_ID,
          client_secret: process.env.FB_CLIENT_SECRET,
          redirect_uri: `${process.env.WEB_HOST}/${opts.locale}fb`,
          code: opts.code,
        },
      }
    )
    data.fb = fbData.data

    const fbUserDetail = await axios.get('https://graph.facebook.com/me', {
      params: {
        fields: ['id', 'email', 'first_name', 'last_name'].join(','),
        access_token: data.fb.access_token,
      },
    })

    const expiresIn = 10
    const accessToken = jsonwebtoken.sign(
      {
        picture: 'https://github.com/nuxt.png',
        username: fbUserDetail.data.first_name,
        scope: ['member'],
      },
      'dummy',
      {
        expiresIn,
      }
    )
    data.locale.accessToken = accessToken

    return data
  },
  async postRun(req, res, runResult) {
    res.json(runResult)
  },
})

router.post('/login', auth, async (req, res, next) => {
  doLogin._controller(req, res)
})
let doLogin = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.email = req.body.email
    opts.password = req.body.password
    opts.type = req.body.type
    return opts
  },
  async validOpts(opts) {
    if (!opts.email || !opts.password)
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
  },
  async run(opts) {
    const data = await knex.raw(
      `select * from ${opts.type} where email = ? and password=?`,
      [opts.email, opts.password]
    )
    if (!data[0][0])
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
    const expiresIn = 10000
    const user = data[0][0]
    const accessToken = jsonwebtoken.sign(
      {
        picture: 'https://github.com/nuxt.png',
        username: data[0][0].name,
        scope: [opts.type],
      },
      'dummy',
      {
        expiresIn,
      }
    )
    // await new Promise((resolve) => {
    //   setTimeout(() => {
    //     return resolve(console.error('test timeout'))
    //   }, 2000)
    // })

    return {
      accessToken,
      user,
    }
  },
  async postRun(req, res, runResult) {
    res.json(runResult)
  },
})
router.post('/login/v1', async (req, res, next) => {
  doLoginV1._controller(req, res)
})
let doLoginV1 = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.email = req.body.email
    opts.password = req.body.password
    opts.type = req.body.type
    return opts
  },
  async validOpts(opts) {
    if (!opts.email || !opts.password)
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
  },
  async run(opts) {
    const data = await knex.raw(
      `select * from ${opts.type} where email = ? and password=?`,
      [opts.email, opts.password]
    )
    if (!data[0][0])
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
    const expiresIn = 10000
    const accessToken = jsonwebtoken.sign(
      {
        picture: 'https://github.com/nuxt.png',
        username: data[0][0].name,
        scope: [opts.type],
      },
      'dummy',
      {
        expiresIn,
      }
    )
    return accessToken
  },
  async postRun(req, res, runResult) {
    res.json({ token: { accessToken: runResult } })
  },
})
// [GET] /user
router.get('/user', auth, (req, res, next) => {
  res.json({ user: req.user })
})

// [POST] /logout
router.post('/logout', auth, (req, res, next) => {
  res.json({ status: 'OK' })
})

module.exports = router
