module.exports = function apiProcess(impl) {
  const service = new Service()
  Object.assign(service, impl)
  service.init()
  return service
}
let Service = class service {
  constructor() {}

  /**
   * Sub-class override can this method ofr initialize.
   */
  init() {
    // do nothing
  }

  /**
   * Run middleware before this controller.
   * Sub-class can override this method.
   * @returns {(* | Array)} - A middleware or middleware of Array.
   */
  getMiddlewares() {
    return []
  }

  /**
   * return array of middlewares and controller(run method).
   */
  controller() {
    const self = this
    const middlewares = this.getMiddlewares() || []

    const warppedController = (req, res) => {
      // warpped this
      self._controller(req, res)
    }

    return [].concat(middlewares, warppedController)
  }

  /**
   *
   * @param {*} req
   * @param {*} res
   */
  async _controller(req, res) {
    try {
      // const defaultOpts = await this._getDefaultOpts(req);
      const opts = await this.getOpts(req)
      await this.validOpts(opts)
      const runResult = await this.run(opts, this)
      const result = await this.postRun(req, res, runResult, opts)
      this._processSuccess(res, result) // success response
    } catch (e) {
      console.log(e)
      this._processError(req, res, e) // error response
    }
  }

  /**
   *
   * @param {*} req
   * @param {*} defaultOpts
   */
  async getOpts(req, defaultOpts) {
    return defaultOpts
  }

  /**
   * Sub-class override this method that can validate optopns.
   * If has invalidation parameter of request, throw error.
   * @param {*} opts
   */
  async validOpts(opts) {
    // do nothing
  }

  /**
   * Main service process.
   * Sub-class must override this method.
   * @param {*} opts - from this#validParams method.
   * @returns {*} - response object. If no returns, will return empty json object.
   */
  async run(opts, self) {
    this.throwError('', 'service not implemeted') // throw UnknownError
  }

  /**
   * Optional override.
   * Handle req or res after run() method.
   * @param {*} req
   * @param {*} res
   * @param {*} runResult - return value of run() method.
   * @param {*} opts
   * @returns {*} - response object. If no returns, will return empty json object.
   */
  async postRun(req, res, runResult, opts) {
    return runResult
  }

  /**
   *
   * @param {*} code
   * @param {*} detail
   * @param {*} status
   */
  throwError(code, detail, status) {
    this.throw(code, {
      detail,
      status,
    })
  }

  /**
   *
   * @param {*} code
   * @param {*} opts
   * @param {*} opts.detail -
   * @param {*} opts.originError -
   * @param {*} opts.status -
   */
  throw(code, opts) {
    this.thrower.throwsError(code, opts)
  }

  async _processSuccess(res, result) {
    res.json(result)
    // if (!res.headersSent &&
    //     (res.statusCode == 200 || !res.statusCode)) { // res.statusCode for res.send() enpty argument not end() (not set headersSent to true)
    //   const resBody = {
    //     code: result && result.code,
    //     data: result && result.data || result || {},
    //   };

    //     res.status(200).json(resBody);
    // } else {
    //     res.end();
    // }
  }

  /**
   *
   * @param {*} req
   * @param {*} res
   * @param {*} e -
   * @param {*} e.code - Error Code
   * @param {*} e.data - 詳細資料
   * @param {*} e.status - http status
   */
  _processError(req, res, err) {
    if (!err.statusCode) err.statusCode = 500 // Sets a generic server error status code if none is part of the err

    if (err.shouldRedirect) {
      res.render('myErrorPage') // Renders a myErrorPage.html for the user
    } else {
      res.status(err.statusCode).send(err) // If shouldRedirect is not defined in our error, sends our original err data
    }
  }
}
