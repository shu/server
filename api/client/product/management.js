const router = require('express').Router()

const knex = require(`${rootPath}/server/db`)
// eslint-disable-next-line import/no-dynamic-require
const { uploadImage } = require(`${rootPath}/utils/image`)
const { modifyImage } = require(`${rootPath}/utils/image`)
const apiProcess = require(`${rootPath}/api/apiProcess`)

router.get('/', async (req, res, next) => {
  getProducts._controller(req, res)
})
let getProducts = apiProcess({
  async run(opts) {
    const data = await knex.raw(`select * from product`)

    return data[0]
  },
  async postRun(req, res, runResult) {
    return runResult
  },
})
router.get('/:id', async (req, res, next) => {
  getProduct._controller(req, res)
})
let getProduct = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.id = req.params.id

    return opts
  },
  async run(opts) {
    const data = await knex.raw(`select * from product where id=?`, [opts.id])

    return data[0][0]
  },
  async postRun(req, res, runResult) {
    return runResult
  },
})

router.post('/', uploadImage, async (req, res, next) => {
  addProduct._controller(req, res)
})
let addProduct = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.price = req.body.price
    opts.name = req.body.name
    opts.status = req.body.status
    opts.description = req.body.description
    await modifyImage(req, opts)
    return opts
  },
  async validOpts(opts) {
    if (!opts.price || !opts.name)
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
  },
  async run(opts) {
    const data = await knex.raw(
      `insert into product (
        price,name,status,description,imageUrl
      )
      VALUES (?,?,?,?,?)`,
      [opts.price, opts.name, opts.status, opts.description, opts.imageUrl]
    )
  },
})

router.put('/', uploadImage, async (req, res, next) => {
  updateProduct._controller(req, res)
})
let updateProduct = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.price = req.body.price
    opts.name = req.body.name
    opts.status = req.body.status
    opts.description = req.body.description
    opts.id = req.body.id
    opts.imageUrl = req.body.imageUrl
    await modifyImage(req, opts)
    return opts
  },
  async validOpts(opts) {
    if (!opts.id || !opts.price || !opts.name)
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
  },
  async run(opts) {
    const data = await knex.raw(
      `update product set name=?,price=?,status=? ,description=?,imageUrl=? where id =?`,
      [
        opts.name,
        opts.price,
        opts.status,
        opts.description,
        opts.imageUrl,
        opts.id,
      ]
    )
  },
})

router.delete('/', async (req, res, next) => {
  deleteProduct._controller(req, res)
})
let deleteProduct = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.id = req.query.id

    return opts
  },
  async validOpts(opts) {
    if (!opts.id)
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
  },
  async run(opts) {
    const data = await knex.raw(
      `delete from product
      where id = ?`,
      [opts.id]
    )
  },
})

module.exports = router
