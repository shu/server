const router = require('express').Router()
router.use('/management', require('./management'))

module.exports = router
