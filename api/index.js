const router = require('express').Router()

const { auth } = require(`${rootPath}/server/auth`)
router.use('/auth', require('./auth'))
// all admin api need auth
router.use('/admin', auth, require('./admin'))
router.use('/client', require('./client'))

module.exports = router
