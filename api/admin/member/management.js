const router = require('express').Router()

const knex = require(`${rootPath}/server/db`)
const jsonwebtoken = require('jsonwebtoken')

const apiProcess = require(`${rootPath}/api/apiProcess`)

router.get('/', async (req, res, next) => {
  getMember._controller(req, res)
})
let getMember = apiProcess({
  async run(opts) {
    const data = await knex.raw(`select * from member`)

    return data[0]
  },
  async postRun(req, res, runResult) {
    return runResult
  },
})

router.post('/', async (req, res, next) => {
  addMember._controller(req, res)
})
let addMember = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.email = req.body.email
    // opts.password = req.body.password
    opts.name = req.body.name
    opts.status = req.body.status
    return opts
  },
  async validOpts(opts) {
    if (!opts.email || !opts.name)
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
  },
  async run(opts) {
    const data = await knex.raw(
      `insert into member (
        email,name,status
      )
      VALUES (?,?,?)`,
      [opts.email, opts.name, opts.status]
    )
  },
})

router.put('/', async (req, res, next) => {
  updateMember._controller(req, res)
})
let updateMember = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.email = req.body.email
    opts.password = req.body.password
    opts.name = req.body.name
    opts.status = req.body.status
    opts.id = req.body.id
    return opts
  },
  async validOpts(opts) {
    if (!opts.email || !opts.name)
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
  },
  async run(opts) {
    const data = await knex.raw(
      `update member set name=?,email=?,status=? where id =?`,
      [opts.name, opts.email, opts.status, opts.id]
    )
  },
})

router.delete('/', async (req, res, next) => {
  deleteMember._controller(req, res)
})
let deleteMember = apiProcess({
  async getOpts(req) {
    const opts = {}
    opts.id = req.query.id

    return opts
  },
  async validOpts(opts) {
    if (!opts.id)
      throw { code: 'login.error.INVALID_INPUT', message: 'Invaild input' }
  },
  async run(opts) {
    const data = await knex.raw(
      `delete from member
      where id = ?`,
      [opts.id]
    )
  },
})

module.exports = router
