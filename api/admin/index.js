const router = require('express').Router()
router.use('/member', require('./member'))
router.use('/product', require('./product'))

module.exports = router
