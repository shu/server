const path = require('path')

global.rootPath = path.resolve()
require('dotenv').config()
const express = require('express')
const http = require('http')
const bodyParser = require('body-parser')

const jsonwebtoken = require('jsonwebtoken')
const db = require('./server/db')

// Create app
const app = express()

// Install middleware
// app.use(cookieParser())
app.use(bodyParser.json())

// setting static data to public path ex:http://localhost:4000/account-box.png
app.use(express.static(path.join(__dirname, 'public')))

// -- Routes --
app.use(require('./routes'))

// Error handler

app.use(function (err, req, res, next) {
  console.error(err.message)
  if (!err.statusCode) err.statusCode = 500 // Sets a generic server error status code if none is part of the err

  if (err.shouldRedirect) {
    res.render('myErrorPage') // Renders a myErrorPage.html for the user
  } else {
    res.status(err.statusCode).send(err) // If shouldRedirect is not defined in our error, sends our original err data
  }
})

// function initAuthenticate() {
//   return auth.init();
// }

function initDB() {
  return db.initialize()
}

// function initRouter() {
//   routes.register(app, conf.domainName);
// }

function createHttpServer() {
  if (!process.env.HTTP_PORT) {
    return 'no httpPort'
  }
  http.createServer(app).listen(process.env.PORT || 4000, function () {
    console.log(`Listening on port ${process.env.HTTP_PORT}`)
  })
}

async function startServer() {
  try {
    // await initAuthenticate();
    await initDB()
    // await initRouter();
    await createHttpServer()
    // await createHttpsServer();

    console.log('Web AP Service Server is ready.')
    return 'on'
  } catch (e) {
    return 'off'
  }
}
startServer()

// -- export app --
module.exports = {
  path: '/api/auth',
  handler: app,
}
